'use strict';

const handler = require('../../test-lambda/handler');

describe('when call handler.hello', () => {
	it('should return the success message', async () => {
		const event = {};

		const response = await handler.hello(event);

		expect(response.statusCode).toBe(200);
		expect(response.body).toMatch(`Go Serverless v1.0! Your function executed successfully!`);
	});
});
